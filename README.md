# Test de visualisation graphique

Prototype en "client javascript" pour test et démo essentiellement.  

## Objet et contexte

Visualisation graphique relative à l'épidémie de COVID-19 en France (2020)

## Origine des données

Données principales ouvertes et accessibles sur le site [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/donnees-hospitalieres-relatives-a-lepidemie-de-covid-19/).  
Données mise à disposition par Santé Publique France [santepubliquefrance.fr](https://www.santepubliquefrance.fr/)

## [Version en ligne](https://jgorene.gitlab.io/covid19-stats)

## Note pour réutilisation

Cette application n'est qu'une démonstration et doit être utilisée comme telle ou encore à des fins d'apprentissage ou de manipulation expérimentale. En tant que tel, je n'offre aucune garantie quant à son utilisation dans un environnement de production. Vous pouvez la copier et bien sûr l'améliorer le cas échéant mais vous le faites à vos risques et périls.

## Plateforme de développement

[Node.js](https://nodejs.org/en/)

## Éditeurs

[Sublimetest 3](https://www.sublimetext.com/)
[Visual studio code](https://code.visualstudio.com/)

## Librairies javascript utilisées

[d3.js](https://d3js.org/)  
[papaparse.js](https://www.papaparse.com/)  
[moment.js](https://momentjs.com/)  

## Librairie graphique

[dygraphs](https://github.com/danvk/dygraphs)

## CSS

[w3.css](https://www.w3schools.com/w3css/)

## Icônes

[gg.css](https://css.gg/)
